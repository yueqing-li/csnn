from cerebellar_model import *
from spikingjelly import visualizing
from matplotlib import pyplot as plt
from arm import *

num_epochs = 1
sim_frame = 12000
q0 = np.array([-pi/6, pi/3])
cere_net = CerebellarModel()
cere_net = cere_net.to(device)
arm      = TwoLinkArm(q0=q0)

# generate desired trajectory
T = 1.0
r = 0.5
xc = np.sqrt(3) - r
yc = 0
# control frequency
fre = 1000.
dt = 1/fre

# train loop
optimizer = torch.optim.SGD(cere_net.gc2pc.parameters(), lr=lr, momentum=0.)
print("current device: "+str(device))

for epoch in range(num_epochs):
    cere_net.resetNeurons()
    arm.resetStates(q0=q0)
    # plot data
    t_list = []
    os_list = [list(), list()]
    os_act_list = [list(), list()]
    js_list = [list(), list()]
    js_act_list = [list(), list()]
    weight = []
    tau_list = [list(), list()]
    v_list = []
    s_list = []
    cere_net.stdp_learner.enable()
    with torch.no_grad():
        for i in range(sim_frame):
            # sim time
            t = dt*float(i)
            # operate space
            xt = xc + r*np.cos(2*np.pi*t/T)
            yt = yc + r*np.sin(2*np.pi*t/T)
            dxt = (-2*np.pi*r/T)*np.sin(2*np.pi*t/T)
            dyt = (2*np.pi*r/T)*np.cos(2*np.pi*t/T)
            # joint space
            q_des, dq_des = arm.inverseKinematics(np.array([xt,yt]), np.array([dxt, dyt]))
            q_act, dq_act = arm.armStates()
            # train
            input_data = np.array([[q_des[0], dq_des[0], q_act[0], dq_act[0]],
                                [q_des[1], dq_des[1], q_act[1], dq_act[1]]])
            
            optimizer.zero_grad()
            out_spike = cere_net(input_data, teach_info=True)
            cere_net.stdp_learner.step(on_grad=True)
            optimizer.step()
            #cere_net.mf2gc.weight.data.clamp_(0.000001,5)

            # decode spikes
            tau = decodeDCN(out_spike)
            (x_act,y_act) = arm.detailedSimulation(tau, t = dt)

            # record
            t_list.append(t)
            os_list[0].append(xt)
            os_list[1].append(yt)
            os_act_list[0].append(x_act)
            os_act_list[1].append(y_act)
            js_list[0].append(q_des[0])
            js_list[1].append(q_des[1])
            js_act_list[0].append(q_act[0])
            js_act_list[1].append(q_act[1])
            weight.append(cere_net.gc2pc.weight.data[50,50:156].clone().cpu().detach().numpy())
            tau_list[0].append(tau[0])
            tau_list[1].append(tau[1])
            # neuron
            v_list.append(cere_net.pc_neurons.v[0,0:-1:10].unsqueeze(0))
            s_list.append(cere_net.pc_spikes[0,0:-1:10].unsqueeze(0))

            if (i+1)%12000 == 0:
                print ('Epoch [%d/%d], Step [%d/%d].'
                        %(epoch+1, num_epochs, i+1, sim_frame))
                # print('Time elasped:', time.time()-start_time)
    cere_net.stdp_learner.disable()
    cere_net.stdp_learner.reset()
    err = (np.linalg.norm(np.array(os_list[0]) - np.array(os_act_list[0]))
         + np.linalg.norm(np.array(os_list[1]) - np.array(os_act_list[1])))/sim_frame
    print ('Epoch [%d/%d], Error [%s].'
                        %(epoch+1, num_epochs, err))



plt.figure("OS")
plt.plot(os_list[0], os_list[1])
plt.plot(os_act_list[0], os_act_list[1])
plt.figure("JS")
plt.subplot(2,1,1)
plt.plot(t_list, js_list[0])
plt.plot(t_list, js_act_list[0])
plt.subplot(2,1,2)
plt.plot(t_list, js_list[1])
plt.plot(t_list, js_act_list[1])
plt.figure("weight")
plt.plot(t_list,weight)
plt.figure("tauque")
plt.plot(t_list,tau_list[0])
plt.plot(t_list,tau_list[1])

# spike and voltage plot 
s_list = torch.cat(s_list)
v_list = torch.cat(v_list)

figsize = (24, 16)
dpi = 200
visualizing.plot_2d_heatmap(array=v_list.cpu().detach().numpy(),
                            title='PC Membrane Potentials',
                            xlabel='simulation step',
                            ylabel='neuron index',
                            int_x_ticks=True, x_max=T,
                            figsize=figsize, dpi=dpi)

visualizing.plot_1d_spikes(spikes=s_list.cpu().detach().numpy(),
                           title='PC Out Spikes',
                           xlabel='simulation step',
                           ylabel='neuron index',
                           figsize=figsize,
                           dpi=dpi)

plt.show()

